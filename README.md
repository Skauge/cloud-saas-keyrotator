**Table of Contents**

[[_TOC_]]

# Cloud SaaS Keyrotator

A Rust keyrotator for various cloud providers.
(AWS is first and only for now though).


## TODO
1. Read and parse the `~/.aws/credentials` file
   1. [x] ~Parse INI file.~
      - [x] ~Confirm with Property-Based testing.~
   2. [x] ~Read `~/.aws/credentials`.~
2. [ ] Find rust lib to connect to aws
   - [ ] Consider [rusoto](https://rusoto.org/) usability.
3. [ ] Write code to check lifetime of all keys in file
   - [ ] Maybe add config for exclusions?
4. [ ] Rotate all keys above a certain age
   - [ ] Maybe add config for setting age?

# Usage
For now:

1. `git pull`
2. `cd` into repo.
3. `cargo run -- --help`

## Example

<details>
<summary markdown="span">`bash` example usage</summary>

```bash
$ date && echo && cargo test && echo && cargo run -- -i "x", "v" -vv -I 'y' --file-path ./credentials
Sat  4 Apr 13:59:42 CEST 2020

    Finished test [unoptimized + debuginfo] target(s) in 0.02s
     Running target/debug/deps/cloud_saas_keyrotator-8ef7f9a64cdb34e4

running 1 test
test tests::proptest_ini_file_contents_parsing ... ok

test result: ok. 1 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out


    Finished dev [unoptimized + debuginfo] target(s) in 0.02s
     Running `target/debug/cloud_saas_keyrotator -i x, v -vv -I y --file-path ./credentials`
Cli {
    verbose: 2,
    file_path: Some(
        "./credentials",
    ),
    key_include_pattern: Some(
        [
            "x,",
            "v",
        ],
    ),
    key_exclude_pattern: Some(
        [
            "y",
        ],
    ),
}
Specified file "/home/x10an14/Documents/gitlab/cloud-saas-keyrotator/credentials" found and read!
Section '[default]' found with 'key = value' pairs;
	aws_access_key_id = AKIAIOSFODNN7EXAMPLE
	aws_secret_access_key = wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
Section '[web-dev]' found with 'key = value' pairs;
	aws_access_key_id = OTHER_KEY_ID
	aws_secret_access_key = OTHER_ACCESS_KEY
Section '[web-prod]' found with 'key = value' pairs;
	aws_access_key_id = ANOTHER_KEY_ID
	aws_secret_access_key = ANOTHER_ACCESS_KEY
```
</details>

## AWS comment
Profile specific access keys for AWS are stored inside of ~/.aws/credentials in INI format.
This file may contain several profiles, but (as the INI format standard specifies) can only contain _one_ section named `default`.

### Example of a credentials file

`~/.aws/credential` example (none of the keys are real).

```bash
$ cat ~/.aws/credential
[default]
aws_access_key_id=AKIAIOSFODNN7EXAMPLE
aws_secret_access_key=wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY

[web-dev]
aws_access_key_id=OTHER_KEY_ID
aws_secret_access_key=OTHER_ACCESS_KEY

[web-prod]
aws_access_key_id=ANOTHER_KEY_ID
aws_secret_access_key=ANOTHER_ACCESS_KEY
```

### Example for profile usage
```
aws eks list-clusters --profile web-prod
```

### Example awscli command for handling aws access keys
NB! In the example profile is matching the value of the bracket in ~/.aws/credential
All keys here are fake from aws examples

1. List access keys;

    ```
    aws iam list-access-keys --profile susan
    ```

2. Create new access key;

    ```
    aws iam create-access-key --profile susan

    # This returns
    {
        "AccessKey": {
            "UserName": "susan",
            "Status": "Active",
            "CreateDate": "2020-04-06T13:14:35Z"
            "SecretAccessKey": "wJalrXUtnFEMI/K7MDENG/bPxRfiCYzEXAMPLEKEY",
            "AccessKeyId": “AKIAIOSFODNN7EXAMPLE"
        }
    }
    ```

3. Check that you have two active keys;

    ```
    aws iam list-access-keys --profile susan

    # Should return something like this
    {
        "AccessKeyMetadata": [
            {
                "UserName": "susan",
                "AccessKeyId": "AKIAWCPL6OGEXAMPLE",
                "Status": "Active",
                "CreateDate": "2020-04-06T14:02:28Z"
            },
            {
                "UserName": "susan",
                "AccessKeyId": "“AKIAIOSFODNN7EXAMPLE",
                "Status": "Active",
                "CreateDate": "2020-04-06T13:14:35Z"
            }
        ]
    }
    ```

5. Change keys trough `aws` CLI;

    ```
    aws configure --profile susan set aws_access_key_id "NEW_ACCESS_KEY_ID"
    aws configure --profile susan set aws_secret_access_key "new_secret_access_key"
    ```

6. Deactivate old key;

    ```
    aws iam update-access-key --access-key-id AKIAIOSFODNN7EXAMPLE --status Inactive --profile susan
    ```

7. When you have tested your key and are sure it works. You can delete the inactive key;

    ```
    aws iam delete-access-key --access-key-id AKIAIOSFODNN7EXAMPLE --profile susan
    ```

# Developer Usage
## Required `cargo install`s

1. `cargo install cargo-edit`; Installs `cargo add [--dev]` and `cargo rm` for crate dependencies manipulation.
   - https://crates.io/crates/cargo-edit
2. `cargo install cargo-audit`; Installs `cargo audit` command for checking Crate vulnerabilities.
   - https://crates.io/crates/cargo-audit
3. `rustup component add rustfmt`; Installs `cargo fmt` command for linting purposes.
4. `cargo install cargo-make`; Installs `cargo make` which executes (by default) `[tasks.X]`s in `Makefile.toml`.
   - https://crates.io/crates/cargo-make

These and more found at (this long blog post)[https://users.rust-lang.org/t/list-of-crates-that-improves-or-experiments-with-rust-but-may-be-hard-to-find/17806].

## Cargo must-run commands

Each and any of the below commands are recommended to be executed _before_ their `(read: ...)` comment specifies, but _**all**_ must be acted upon if failing before merging a `git push`ed branch into `master` branch.

1. `cargo make fmt` (read: before `git commit`)
2. `cargo test` (read: before `git commit`)
3. `cargo make audit_check` (read: before `git push`)

