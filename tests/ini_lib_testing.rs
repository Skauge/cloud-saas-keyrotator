mod tests {
	// Std-lib imports
	use std::collections::HashMap;

	// Non-std lib imports
	use ini::Ini;
	use proptest::prelude as pt;
	use unindent::Unindent;

	// https://stackoverflow.com/a/32956502
	const INI_STRING_SECTION_PROPTEST_REGEX: &'static str =
		concat!("[a-zA-Z]", "[a-zA-Z0-9-_/=?!]*");
	const INI_STRING_KEYVALUE_PROPTEST_REGEX: &'static str = concat!("[a-zA-Z0-9-_/=?!]*");
	const AWS_KEY_ID: &'static str = "aws_access_key_id";
	const AWS_KEY_SECRET: &'static str = concat!("aws_secret_access_key");
	pt::proptest! {
		#[test]
		fn proptest_ini_file_contents_parsing(
			section_name in INI_STRING_SECTION_PROPTEST_REGEX,
			key_id in INI_STRING_KEYVALUE_PROPTEST_REGEX,
			key_secret in INI_STRING_KEYVALUE_PROPTEST_REGEX,
		) {
			// Build up input(read: test data)
			let mut data_hashmap = HashMap::new();

			// general
			let mut gen_map = HashMap::new();
			gen_map.insert(AWS_KEY_ID, key_id.as_str());
			gen_map.insert(AWS_KEY_SECRET, key_secret.as_str());
			data_hashmap.insert(section_name.as_str(), gen_map);
			// println!("{:?}", &data_hashmap);

			// Read input and parse it
			let mut input_test_ini_contents_string: String = String::new();
			for (section_name, section_data) in &data_hashmap {
				let mut section_string = format!("[{}]", section_name);
				for (key, value) in section_data {
					section_string = format!("{}\n{}={}", section_string, key, value);
				}
				input_test_ini_contents_string =
					format!("{}\n{}\n", input_test_ini_contents_string, section_string);
			}
			// println!("{}", &input_test_ini_contents_string);
			let parsed_string = Ini::load_from_str(&input_test_ini_contents_string.unindent()).unwrap();

			// Assert section name is present (as the only section parsed) and correctly parsed
			pt::prop_assert_eq!(
				parsed_string.len(),
				1,
				"There's more than the one expected section in the parsed INI file string!");

			let expected_section_name = Some(section_name.as_str());
			let expected_sections: Vec<_> = parsed_string
				.section_all(expected_section_name)
				.collect();

			// Assert both key-value pairs are present and correctly parsed
			pt::prop_assert_eq!(expected_sections.len(),
				1,
				"There's more than one INI file section extracted matching the section name!");
			let expected_section = expected_sections[0].to_owned();
			pt::prop_assert_eq!(
				expected_section.len(),
				2,
				"There's more than the two expected keys within said INI section!");
			pt::prop_assert_eq!(
				expected_section.get(AWS_KEY_ID),
				Some(key_id.as_str()),
				"Key-value pair for '{}' doesn't match!",
				AWS_KEY_ID);
			pt::prop_assert_eq!(
				expected_section.get(AWS_KEY_SECRET),
				Some(key_secret.as_str()),
				"Key-value pair for '{}' doesn't match!",
				AWS_KEY_SECRET);
		}
	}
}
