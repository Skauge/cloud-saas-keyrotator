mod tests {
	// Import imports from outer scope (read: this file)
	#[cfg(test)]
	use cloud_saas_keyrotator::section_matches_regex_inputs;

	#[test]
	fn test_has_match_has_non_existing_string_returns_false() {
		let key_include_pattern: Vec<String> = vec![
			"(www-)(.*)".to_string(),
			"nei-dev".to_string(),
			".*-dev".to_string(),
		];

		let result = section_matches_regex_inputs(&key_include_pattern, "hell-no");

		assert_eq!(
			result, false,
			"Should have no matches, but result has matching elements!"
		)
	}

	#[test]
	fn test_has_match_has_matching_string_returns_true() {
		let key_include_pattern: Vec<String> = vec![
			"(www-)(.*)".to_string(),
			"nei-dev".to_string(),
			"nei-dev".to_string(),
			"nei-dev".to_string(),
			".*-dev".to_string(),
		];

		let result: bool = section_matches_regex_inputs(&key_include_pattern, "www-dev");

		assert!(result, "Should have match on valid and expecting input!");
	}
}
