// Std-lib imports
use std::collections::HashSet;
use std::error::Error;
use std::option::Option;
use std::path::{Path, PathBuf};
use std::process;

// Non-std lib imports
use dirs;
use ini::Ini;
use structopt::{
	clap::AppSettings::{ColorAuto, ColoredHelp},
	StructOpt,
};

// Import imports from this crate
use cloud_saas_keyrotator::section_matches_regex_inputs;

#[derive(Debug, StructOpt)]
#[structopt(setting(ColorAuto), setting(ColoredHelp), about)]
struct Cli {
	/// Verbose mode (-v, -vv, -vvv, etc.).
	#[structopt(short, long, parse(from_occurrences))]
	verbose: u64,

	/// Path to specific '.aws/credentials'-like file to operate on
	///
	/// Defaults to environment variable `$USER` if no input is given.
	/// Cannot be used with `--user`.
	#[structopt(short = "f", long = "file-path")]
	file_path: Option<PathBuf>,

	/// A list of one or more regex patterns for '[<key>]' section(s) to match on in credentials file.
	/// Inputs given to this flag is evaluated _before_ the inputs given to `-I`.
	///
	/// Example: Include only INI sections whose names start with 'web' or 'www' substrings; `-i "web.+", "www.+"`
	#[structopt(short = "i", long = "regex-include")]
	section_include_patterns: Vec<String>,

	/// A list of one or more regex patterns for '[<key>]' section(s) to ignore in credentials file.
	/// Inputs given to this flag is evaluated _after_ the inputs given to `-i`.
	///
	/// Example: Exclude all INI sections whose names start with 'web' or 'www' substrings; `-I "web.+", "www.+"`
	#[structopt(short = "I", long = "regex-ignore")]
	section_exclude_patterns: Vec<String>,
}

fn main() -> Result<(), Box<dyn Error>> {
	let cli = Cli::from_args();
	let verbosity_level = cli.verbose;
	if verbosity_level >= 2 {
		eprintln!("Debug2:CLI struct; {:#?}", &cli);
	}

	// Ascertain which file path is specified by commandline arguments
	let target_file: PathBuf;
	if let Some(input_path_buf) = &cli.file_path {
		target_file = input_path_buf.canonicalize()?;
	} else {
		if let Some(home_dir) = dirs::home_dir() {
			target_file = Path::new(&home_dir)
				.join(".aws")
				.join("credentials")
				.canonicalize()?;
		} else {
			eprintln!(
				"FATAL_ERROR:No `--file-path` specified, and unable to discern current user's home directory!"
			);
			process::exit(1);
		}
	}
	if verbosity_level >= 1 {
		eprintln!(
			"Debug1:Specified file path == '{:#?}'",
			target_file.as_os_str()
		);
	}

	// Ensure file exists and is readable
	let target_file_contents = match Ini::load_from_file(&target_file) {
		Ok(file_contents) => file_contents,
		Err(_) => {
			eprintln!(
				"FATAL_ERROR: No readable file found at '{}'. {} {}",
				&target_file.display(),
				"Ensure that file exists and is readable for user executing",
				std::env::current_exe()?.display(),
			);
			process::exit(1);
		}
	};
	if verbosity_level >= 2 {
		eprintln!(
			"Debug2:Specified file {:#?} found and read!",
			target_file.as_os_str()
		);
		if verbosity_level >= 3 {
			eprintln!("Debug3:Contents of specfied file;",);
			// Print shit found in file
			for section in target_file_contents.sections() {
				if let Some(section_name) = section {
					eprintln!(
						"Debug3:\tSection '[{}]' found with 'key = value' pairs;",
						section_name
					);
					if let Some(section_data) = target_file_contents.section(Some(section_name)) {
						for (key, value) in section_data.iter() {
							eprintln!("Debug3:\t\t{} = {}", key, value);
						}
					}
				}
			}
		}
	}

	// We don't care here that an INI section may have a duplicate in its own file, thus a HashSet
	// is used.
	//
	// PS: .filter_map safely unpacks the Option<&str> that .sections() returns an iterator of.
	let section_names: HashSet<&str> = target_file_contents
		.sections()
		.filter_map(|section_name| section_name)
		.filter(|&name| {
			// Include all section name strings which match regex input(s) - if any - given to program.
			if cli.section_include_patterns.is_empty() {
				return true;
			}
			section_matches_regex_inputs(&cli.section_include_patterns, &name)
		})
		.filter(|&name| {
			// Exclude all section name strings which match regex input(s) - if any - given to program.
			if cli.section_exclude_patterns.is_empty() {
				return true;
			}
			!section_matches_regex_inputs(&cli.section_exclude_patterns, &name)
		})
		.collect();
	// dbg!("{:#?}", section_names);
	Ok(())
}
