// std-lib imports

// Non-std lib imports
use regex::RegexSet;

pub fn section_matches_regex_inputs(
	include_patterns: &Vec<String>,
	section_name: &str,
) -> bool {
	let set = match RegexSet::new(include_patterns) {
		Ok(set) => set,
		Err(_) => {
			eprintln!(
				"FATAL_ERROR: Invalid regex string(s) sent in as input;\n\t{:#?}",
				include_patterns
			);
			std::process::exit(1);
		}
	};
	// println!("Matches: {:?}", matches);
	set.matches(&section_name).matched_any()
}
